{-# LANGUAGE OverloadedStrings #-}

module Errors
  ( handleDBError
  ) where

import qualified Hasql.Session as Session

import Network.HTTP.Types
import System.Log.Logger

import Control.Monad.IO.Class

import Web.Scotty

handleDBError qerr = 
  case qerr of
    Session.ClientError err -> do
      liftIO $ errorM "pastor.db" $ show err
      status status500
    Session.ResultError err ->
      case err of
        Session.UnexpectedAmountOfRows i -> do
          liftIO $ debugM "pastor.db" "not found"
          status status404
          text "404 Not Found"
