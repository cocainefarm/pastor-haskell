{-# LANGUAGE OverloadedStrings #-}

module Statements 
  ( createShortLinkTableStatement
  , createFileTableStatement
  , postShortLinkStatement
  , getShortLinkStatement
  , deleteShortLinkStatement
  , putShortLinkStatement
  , postFileStatement
  , getFileStatement
  , deleteFileStatement
  , putFileStatement
  ) where


import Prelude
import Data.Int
import qualified Data.Text as T
import Data.Functor.Contravariant
import Hasql.Statement (Statement(..))
import qualified Hasql.Decoders as Decoders
import qualified Hasql.Encoders as Encoders

import qualified Data.ByteString as BS

import Contravariant.Extras.Contrazip

import Network.URL

createShortLinkTableStatement :: Statement () ()
createShortLinkTableStatement = Statement sql Encoders.noParams Decoders.noResult True where
  sql = "CREATE TABLE IF NOT EXISTS shortUrl (short TEXT PRIMARY KEY, adminKey TEXT, url TEXT)"

createFileTableStatement :: Statement () ()
createFileTableStatement = Statement sql Encoders.noParams Decoders.noResult True where
  sql = "CREATE TABLE IF NOT EXISTS fileStore (short TEXT PRIMARY KEY, adminKey TEXT, isBinary BOOL, content TEXT, charset TEXT, fileExt TEXT, fileMime TEXT)"


-------------------------------------------------------------------------------------------
  --
  --  Short Link Statements
  --

postShortLinkStatement :: Statement (T.Text, T.Text, T.Text) ()
postShortLinkStatement = Statement sql encoder Decoders.noResult True where
  sql = "INSERT INTO shortUrl(short, adminKey, url) VALUES ($1, $2, $3)"
  encoder = contrazip3
    (Encoders.param (Encoders.nonNullable Encoders.text))
    (Encoders.param (Encoders.nonNullable Encoders.text))
    (Encoders.param (Encoders.nonNullable Encoders.text))

getShortLinkStatement :: Statement T.Text T.Text
getShortLinkStatement = Statement sql encoder decoder True where
  sql = "SELECT url FROM shortUrl WHERE short = $1"
  encoder = Encoders.param (Encoders.nonNullable Encoders.text)
  decoder = Decoders.singleRow (Decoders.column (Decoders.nonNullable Decoders.text))

putShortLinkStatement :: Statement (T.Text, T.Text, T.Text) T.Text
putShortLinkStatement = Statement sql encoder decoder True where
  sql = "UPDATE shortUrl SET url = $3 WHERE short = $1 AND adminKey = $2 RETURNING short"
  encoder = contrazip3
    (Encoders.param (Encoders.nonNullable Encoders.text))
    (Encoders.param (Encoders.nonNullable Encoders.text))
    (Encoders.param (Encoders.nonNullable Encoders.text))
  decoder = Decoders.singleRow (Decoders.column (Decoders.nonNullable Decoders.text))

deleteShortLinkStatement :: Statement (T.Text, T.Text) ()
deleteShortLinkStatement = Statement sql encoder decoder True where
  sql = "DELETE FROM shortUrl WHERE short = $1 AND adminKey = $2"
  encoder = contrazip2
    (Encoders.param (Encoders.nonNullable Encoders.text))
    (Encoders.param (Encoders.nonNullable Encoders.text))
  decoder = Decoders.noResult


-------------------------------------------------------------------------------------------
  --
  --  File Storage Statements
  --

postFileStatement :: Statement (T.Text, T.Text, Bool, Maybe T.Text, T.Text, T.Text) ()
postFileStatement = Statement sql encoder Decoders.noResult True where
  sql = "INSERT INTO fileStore(short, adminKey, isBinary, content, fileExt, fileMime) VALUES ($1, $2, $3, $4, $5, $6)"
  encoder = contrazip6
    (Encoders.param (Encoders.nonNullable Encoders.text))
    (Encoders.param (Encoders.nonNullable Encoders.text))
    (Encoders.param (Encoders.nonNullable Encoders.bool))
    (Encoders.param (Encoders.nullable Encoders.text))
    (Encoders.param (Encoders.nonNullable Encoders.text))
    (Encoders.param (Encoders.nonNullable Encoders.text))

getFileStatement :: Statement T.Text (T.Text, Bool, Maybe T.Text, T.Text, T.Text)
getFileStatement = Statement sql encoder decoder True where
  sql = "SELECT short, isBinary, content, fileExt, fileMime FROM fileStore WHERE short = $1"
  encoder = Encoders.param (Encoders.nonNullable Encoders.text)
  decoder = Decoders.singleRow row where
    row =
      (,,,,) <$>
      Decoders.column (Decoders.nonNullable Decoders.text) <*>
      Decoders.column (Decoders.nonNullable Decoders.bool) <*>
      Decoders.column (Decoders.nullable Decoders.text) <*>
      Decoders.column (Decoders.nonNullable Decoders.text) <*>
      Decoders.column (Decoders.nonNullable Decoders.text)

putFileStatement :: Statement (T.Text, T.Text, Bool, Maybe T.Text, T.Text, T.Text) T.Text
putFileStatement = Statement sql encoder decoder True where
  sql = "UPDATE fileStore SET isBinary = $3, content = $4, fileExt = $5, fileMime = $6 WHERE short = $1 AND adminKey = $2 RETURNING short"
  encoder = contrazip6
    (Encoders.param (Encoders.nonNullable Encoders.text))
    (Encoders.param (Encoders.nonNullable Encoders.text))
    (Encoders.param (Encoders.nonNullable Encoders.bool))
    (Encoders.param (Encoders.nullable Encoders.text))
    (Encoders.param (Encoders.nonNullable Encoders.text))
    (Encoders.param (Encoders.nonNullable Encoders.text))
  decoder = Decoders.singleRow (Decoders.column (Decoders.nonNullable Decoders.text))

deleteFileStatement :: Statement (T.Text, T.Text) ()
deleteFileStatement = Statement sql encoder decoder True where
  sql = "DELETE FROM fileStore WHERE short = $1 AND adminKey $2"
  encoder = contrazip2
    (Encoders.param (Encoders.nonNullable Encoders.text))
    (Encoders.param (Encoders.nonNullable Encoders.text))
  decoder = Decoders.noResult
