module Sessions 
  ( shortyGen
  , createTables
  , postShortLink
  , getShortLink
  , deleteShortLink
  , putShortLink
  , postFile
  , getFile
  , deleteFile
  , putFile
  ) where

import Hasql.Session (Session)
import qualified Hasql.Session as Session 

import qualified Data.Text as T
import qualified Data.ByteString as BS
import Text.StringRandom
import System.Random

import Control.Monad
import Control.Monad.IO.Class
import Network.URL

import Statements


alphaNum = ['a' .. 'z'] ++ ['A'..'Z'] ++ ['0'..'9']
randomElement l = randomRIO (0, length l - 1) >>= \d -> return (l !! d)
shortyGen = replicateM 6 (randomElement alphaNum)

createTables :: Session ()
createTables = Session.statement () createShortLinkTableStatement >> Session.statement () createFileTableStatement

postShortLink :: String -> String -> String -> Session ()
postShortLink url token adminKey =
  Session.statement (T.pack token, T.pack adminKey, T.pack url) postShortLinkStatement

getShortLink :: String -> Session T.Text
getShortLink token =
  Session.statement (T.pack token) getShortLinkStatement

putShortLink :: String -> String -> String -> Session T.Text
putShortLink url token adminKey =
  Session.statement (T.pack token, T.pack adminKey, T.pack url) putShortLinkStatement

deleteShortLink :: String -> String -> Session ()
deleteShortLink token adminKey =
  Session.statement (T.pack token, T.pack adminKey) deleteShortLinkStatement



postFile :: String -> String -> Bool -> Maybe T.Text -> T.Text -> T.Text -> Session ()
postFile token adminKey isBinary content fileExt fileMime =
  Session.statement (T.pack token, T.pack adminKey, isBinary, content, fileExt, fileMime) postFileStatement

getFile :: String -> Session (T.Text, Bool, Maybe T.Text, T.Text, T.Text)
getFile token =
  Session.statement (T.pack token) getFileStatement

putFile :: String -> String -> Bool -> Maybe T.Text -> T.Text -> T.Text -> Session T.Text
putFile token adminKey isBinary content fileExt fileMime =
  Session.statement (T.pack token, T.pack adminKey, isBinary, content, fileExt, fileMime) putFileStatement

deleteFile :: String -> String -> Session ()
deleteFile token adminKey =
  Session.statement (T.pack token, T.pack adminKey) deleteFileStatement
