{-# LANGUAGE OverloadedStrings #-}

module Route.ShortLinks 
  ( shortLinkGet
  , shortLinkPost
  , shortLinkDelete
  , shortLinkPut
  ) where

import qualified Data.Text.Lazy as TL

import Web.Scotty

import Hasql.Session (Session)
import Hasql.Statement (Statement(..))
import qualified Hasql.Connection as Connection
import qualified Hasql.Session as Session

import Sessions
import Errors

import Data.Text
import Data.Maybe

import Control.Monad.IO.Class

import Network.URL
import Network.HTTP.Types
import System.Log.Logger

shortLinkPost conn = do
    vurl <- param "url"
    liftIO $ debugM "pastor.web" vurl
    token <- liftIO shortyGen
    adminKey <- liftIO shortyGen
    res <- liftIO $ Session.run (postShortLink vurl token adminKey) conn
    case res of
      Left (Session.QueryError qsql qres qerr) -> handleDBError qerr
      Right _ -> do
        referer <- header "Host"
        html $ TL.concat ["Token: ", TL.pack adminKey, "\n", "URL: https://", fromMaybe "couldn't find host" referer, "/u/", TL.pack token, "\n"]

shortLinkGet conn = do
    token <- param "env"
    res <- liftIO $ Session.run (getShortLink token) conn
    case res of
      Left (Session.QueryError qsql qres qerr) -> handleDBError qerr
      Right resUrl -> do
        liftIO $ debugM "pastor.web" $ unpack resUrl
        redirect $ TL.fromStrict resUrl
 
shortLinkDelete conn = do
    token <- param "env"
    adminKey <- param "token"
    res <- liftIO $ Session.run (deleteShortLink token adminKey) conn
    case res of
      Left (Session.QueryError qsql qres qerr) -> handleDBError qerr
      Right _ -> do
        liftIO $ debugM "pastor.web" "url deleted"
        status status200 
 
shortLinkPut conn = do
    vurl <- param "url"
    liftIO $ debugM "pastor.web" vurl
    token <- param "env"
    adminKey <- param "token"
    res <- liftIO $ Session.run (putShortLink vurl token adminKey) conn
    case res of
      Left (Session.QueryError qsql qres qerr) -> handleDBError qerr
      Right _ -> do
        referer <- header "Host"
        html $ TL.append "https://" $ TL.append (fromMaybe "couldn't find host" referer) $ TL.append "/u/" (TL.pack token)

