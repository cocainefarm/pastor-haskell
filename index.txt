

                             _ __   __ _ ___| |_ ___  _ __ 
                            | '_ \ / _` / __| __/ _ \| '__|
                            | |_) | (_| \__ \ || (_) | |
                            | .__/ \__,_|___/\__\___/|_|
                            |_|
                       
                       The pastebin that hopefully doesn't suck


Usage
=====

Files
-----
 Upload

  Upload a single file.
  The first token is needed if you want to change or delete the paste later:
  -----------------------------------------------------------------------------------
  $ curl -F c=@vim.png https://<your-domain-here>/
  a2fm8 https://<your-domain-here>/xc78CK
  -----------------------------------------------------------------------------------
  

  You can also upload mutiple files at once and stream from stdin:
  -----------------------------------------------------------------------------------
  $ echo something.txt | curl -F c=@vim.png -F b=@- https://<your-domain-here>/
  a2fm8 https://<your-domain-here>/xc78CK
  g2m87 https://<your-domain-here>/5jK8a3
  -----------------------------------------------------------------------------------


 Update
  
  Update a paste with a newer version
  -----------------------------------------------------------------------------------
  $ curl -F c=@vim.png -X PUT -d token=a2fm8 https://<your-domain-here>/xc78CK
  -----------------------------------------------------------------------------------


 Delete

  delete the paste
  -----------------------------------------------------------------------------------
  $ curl -X DELETE -d token=a2fm8 https://<your-domain-here>/xc78CK
  -----------------------------------------------------------------------------------


Links
-----
 Create

  Upload a single file.
  The first token is needed if you want to change or delete the link later:
  -----------------------------------------------------------------------------------
  $ curl -d url=https://google.com https://<your-domain-here>/u
  a2fm8 https://<your-domain-here>/u/xc78CK
  -----------------------------------------------------------------------------------


 Update

  Update a paste with a newer version
  -----------------------------------------------------------------------------------
  $ curl -d url=https://google.com -X PUT -d token=a2fm8 https://<your-domain-here>/u
  -----------------------------------------------------------------------------------


 Delete

  delete the paste
  -----------------------------------------------------------------------------------
  $ curl -X DELETE -d token=a2fm8 https://<your-domain-here>/u/xc78CK
  -----------------------------------------------------------------------------------

