{-# LANGUAGE OverloadedStrings #-}
module Main where

import Web.Scotty
import qualified System.Envy as Env
import GHC.Generics


import Prelude
import Data.Int
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C
import Data.Word

import Data.Maybe

import System.IO.Unsafe

import System.Log.Logger

import qualified Data.CaseInsensitive as CI
import Control.Arrow (first)

--import Data.Functor.Contravariant
import qualified Hasql.Connection as Connection
import qualified Hasql.Session as Session
import Sessions

import Data.Monoid (mconcat)

import Routes
import Logger
import Config

main :: IO()
main = do
  updateGlobalLogger rootLoggerName $ setLevel $ read $ pastorLog getConfig
  connection <- Connection.acquire connectionSettings
  case connection of
    Left err -> emergencyM "pastor.db" $ C.unpack $ fromJust err
    Right conn -> do
      infoM "pastor.db" "pastor.db: connected to db"
      Session.run createTables conn
      startWeb conn
  where
    connectionSettings = Connection.settings host port user pass db
      where
        host = pastorPgHost getPGConfig
        port = pastorPgPort getPGConfig
        user = pastorPgUser getPGConfig
        pass = pastorPgPass getPGConfig
        db = pastorPgDb getPGConfig


startWeb :: Connection.Connection -> IO ()
startWeb conn = scotty (pastorPort getConfig) $ do
  middleware $ logger $ pastorLog getConfig
  routes conn

-- addHeaders' :: [Header] -> Response -> Response
-- addHeaders' h = mapResponseHeaders (\hs -> h ++ hs)

--statusBS :: Response -> BS.ByteString
--statusBS = pack . show . statusCode . responseStatus

logCallback = infoM "pastor.web" "web stuff"
