# Pastor
### A pastebin that hopefully doesn't suck

### Configuration
Configuration is done purely through environment variables.

* `PASTOR_PG_{HOST,PORT,USER,PASS,DB}`: configures postgresql connetion details
* `PASTOR_PORT`: Port to listen on for web connections
* `PASTOR_LOG`: Log level to use. Can be `DEBUG`,`INFO`,`ERROR`,`CRITICAL`
* `PASTOR_DATA_DIR`: Directory to store binary files in.

### Usage

#### Files

##### Upload

Upload a single file the first token is needed if you want to change or delete the paste later:
```
$ curl -F c=@vim.png https://<your-domain-here>/
a2fm8 https://<your-domain-here>/xc78CK
```

You can also upload mutiple files at once and stream from stdin:
```
$ echo something.txt | curl -F c=@vim.png -F b=@- https://<your-domain-here>/
a2fm8 https://<your-domain-here>/xc78CK
g2m87 https://<your-domain-here>/5jK8a3
```

##### Update

Update a paste with a newer version
```
$ curl -F c=@vim.png -X PUT -d token=a2fm8 https://<your-domain-here>/xc78CK
```

##### Delete

delete the paste
```
$ curl -X DELETE -d token=a2fm8 https://<your-domain-here>/xc78CK
```


#### Links

##### Create

Upload a single file:
```
$ curl -d url=https://google.com https://<your-domain-here>/u
a2fm8 https://<your-domain-here>/u/xc78CK
```

##### Update

Update a paste with a newer version
```
$ curl -d url=https://google.com -X PUT -d token=a2fm8 https://<your-domain-here>/u
```

##### Delete

delete the paste
```
$ curl -X DELETE -d token=a2fm8 https://<your-domain-here>/u/xc78CK
```
